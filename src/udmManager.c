/*******************************************************************************
 * @file udmManager.c
 * Module: UDM Manager
 * Description:
 *   A manager for "user defined memory" in fluent.
 */
/*******************************************************************************
 * Includes
 ******************************************************************************/
#include "udmManager.h"

/*******************************************************************************
 * Macros
 ******************************************************************************/

/*******************************************************************************
 * type definitions
 ******************************************************************************/

/*******************************************************************************
 * variables
 ******************************************************************************/

/*******************************************************************************
 * Function prototypes
 ******************************************************************************/

/*******************************************************************************
 * Functions
 ******************************************************************************/
/*******************************************************************************
 * Calculates the number of needed UDM store locations.
 *
 * @return The needed number of UDM store locations.
 */
int getNeededUdmSize() {
  int i, currUdmPos = -1, totalUdmPosCnt = 0;

  for (i = 0; i < UDMSTORESIZE; i += 1) {
    if (udmStore[i].udmPos != currUdmPos) {
      currUdmPos = udmStore[i].udmPos;
      totalUdmPosCnt += 1;
    }
  }

  return totalUdmPosCnt;
}

/*******************************************************************************
 * Write a real value to the UDM.
 *
 * @param pDomain The domain pointer needed to get the cell thread.
 * @param pos The UDM position definition, where to write the real value
     (UDM location and zoneId). See @UdmStorePos_t for further information.
 * @param typ The UDM write operation type, see @UdmOpType_t for further
 *   information.
 * @param value The real value to write to the UDM.
 */
void writeRealToUdm(Domain* pDomain, UdmStorePos_t pos, UdmOpType_t type,
  real value) {
#if !RP_HOST
  int n = 0;
  cell_t cell;
  Thread* pThread = Lookup_Thread(pDomain, pos.zoneId);
#endif /* !RP_HOST */

  /* Send value to write in UDM to compute nodes. */
  host_to_node_real_1(value);

#if !RP_HOST
  /* Write values to the cells UDM store of the zone. */
  begin_c_loop(cell, pThread) {
    switch (type) {
      case eUdmOpTypeAllCells:
        /* Write value to all cell UDMs. */
        C_UDMI(cell, pThread, pos.udmPos) = value;
        break;
      case eUdmOpTypeFirstCell:
      default:
        if (n < 1) {
          /* Only write to the cell UDM of the first cell. */
          C_UDMI(cell, pThread, pos.udmPos) = value;
          n = 1;
        } else {
          /* All other cell UDMs will be set to zero. */
          C_UDMI(cell, pThread, pos.udmPos) = 0;
        }
        break;
    }
  } end_c_loop(cell, pThread);
#endif /* !RP_HOST */
}
