/*******************************************************************************
 * @file udmManager.h
 * Module: UDM Manager
 * Description:
 *   A manager for "user defined memory" in fluent.
 */
/*******************************************************************************
 * Includes
 ******************************************************************************/
#include "udf.h"

/** The user of the UDM manager has to create an header file which declares an
 * enum with all needed zone ID`s of the case. The enum type must have the
 * name "ZoneId_t", example:
 * typedef enum ZoneId {
 *   eZoneIdZone1 = 7,
 *   eZoneIdZone2 = 15,
 *   eZoneIdZone3 = 135
 * } ZoneId_t;
 */
#include "zoneIds.h"

#ifndef UDM_MANAGER_H
#define UDM_MANAGER_H

/*******************************************************************************
 * Macros
 ******************************************************************************/

/*******************************************************************************
 * type definitions
 ******************************************************************************/
typedef enum UdmOpType {
  /* Operates on all cells. */
  eUdmOpTypeAllCells = 0,
  /* Operates only on first cell and sets all others to zero. */
  eUdmOpTypeFirstCell
} UdmOpType_t;

typedef struct {
  int udmPos;
  ZoneId_t zoneId;
} UdmStorePos_t;

/*******************************************************************************
 * variables
 ******************************************************************************/
/** The UDM store array has to be defined with the case data in another file,
 * for example:
 * const UdmStorePos_t udmStore[] = {
 *   { .udmPos = 0, .zoneId = eZoneIdZone1 },
 *   { .udmPos = 0, .zoneId = eZoneIdZone2 },
 *   { .udmPos = 0, .zoneId = eZoneIdZone3 },
 *   { .udmPos = 1, .zoneId = eZoneIdZone1 },
 *   { .udmPos = 1, .zoneId = eZoneIdZone2 }
 * };
 * Don't forget to define the size of the array in the UDMSTORESIZE variable,
 * see below.
 */
extern const UdmStorePos_t udmStore[];

/** The value of the UDMSTORESIZE variable has to defined where you define the
 * udmStore array and should be:
 * const int UDMSTORESIZE = sizeof(udmStore) / sizeof(UdmStorePos_t);
 */
extern const int UDMSTORESIZE;

/*******************************************************************************
 * Function prototypes
 ******************************************************************************/
/*******************************************************************************
 * Calculates the number of needed UDM store locations.
 *
 * @return The needed number of UDM store locations.
 */
int getNeededUdmSize();

/*******************************************************************************
 * Write a real value to the UDM.
 *
 * @param pDomain The domain pointer needed to get the cell thread.
 * @param pos The UDM position definition, where to write the real value
 *   (UDM location and zoneId). See @UdmStorePos_t for further information.
 * @param typ The UDM write operation type, see @UdmOpType_t for further
 *   information.
 * @param value The real value to write to the UDM.
 */
void writeRealToUdm(Domain* pDomain, UdmStorePos_t pos, UdmOpType_t type,
  real value);

#endif /* UDM_MANAGER_H */
